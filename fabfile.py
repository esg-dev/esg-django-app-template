from time import time
from fabric.api import env
from fabric.context_managers import prefix, cd
from fabric.contrib.files import exists
from fabric.operations import sudo, run

env.hosts = ['192.168.10.199:2222']
env.user = 'tal'
env.password = 'Aa123456'

APIS_FOLDER = '/home/tal/apis/'
VIRTUALENV_FOLDER = '/home/tal/.virutalenvs/'
MIGRATE_CHANGES_CMD = 'python manage.py migrate'
REMOTE_SUPERVISOR_FOLDER = '/etc/supervisor/conf.d'
LOCAL_BIN_FOLDER = './bin'


def clone_app_cmd(app_name):
    with cd('/home/tal/apis'):
        run('git clone https://gitlab.com/esg-dev/%s.git' % app_name)


def change_last_app_name_cmd(app_name):
    millis = int(round(time() * 1000))
    apis_dir = '/home/tal/apis'
    with cd(apis_dir):
        directory = "/home/tal/apis/%s" % app_name
        if exists(directory):
            new_name = app_name + str(millis)
            sudo('mv %s %s' % (app_name, new_name), user='tal')


def install_requirements_cmd(appname):
    # if exists(VIRTUALENV_FOLDER + appname):
    with prefix("workon {app_name}".format(app_name=appname)):
        run("pip install -r {app_folder}/requirements.txt".format(app_folder=APIS_FOLDER + appname))
        # else:
        #     with prefix("mkvirtualenv {app_name}".format(app_name=appname)):
        #         with cd(APIS_FOLDER + appname):
        #             run("pip install -r requirements.txt")


def migrate_db_changes(appname):
    with cd(APIS_FOLDER + appname):
        with prefix("workon {app_name}".format(app_name=appname)):
            run(MIGRATE_CHANGES_CMD)


def make_bin_file_executable(appname):
    gunicorn_file_path = APIS_FOLDER + appname + '/bin/gunicorn_start.sh'
    sudo("chmod +x {gunicorn_file_path}".format(gunicorn_file_path=gunicorn_file_path), user='tal')


def configure_supervisor(appname):
    supervisor_conf_file = APIS_FOLDER + appname + '/bin/{app_name}.conf'.format(app_name=appname)

    if not exists('/etc/supervisor/conf.d/{app_name}.conf'.format(app_name=appname)):
        sudo('mv {supervisor_conf_file} {remote_supervisor_folder}'.format(supervisor_conf_file=supervisor_conf_file,
                                                                           remote_supervisor_folder=REMOTE_SUPERVISOR_FOLDER),
             user='tal')
        sudo('supervisorctl reread')
        sudo('supervisorctl update')


def restart_supervisorctl_proccess(appname):
    sudo('supervisorctl stop {app_name}'.format(app_name=appname))
    sudo('supervisorctl start {app_name}'.format(app_name=appname))


def restart_nginx():
    sudo('service nginx restart')


def deploy(appname):
    change_last_app_name_cmd(appname)
    clone_app_cmd(appname)
    install_requirements_cmd(appname)
    migrate_db_changes(appname)
    make_bin_file_executable(appname)
    configure_supervisor(appname)
    restart_supervisorctl_proccess(appname)
    restart_nginx()
