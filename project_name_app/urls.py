from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter

admin.autodiscover()

router = ExtendedDefaultRouter()
(

)

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'', include(router.urls)),
                       )
