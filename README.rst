Installation
------------

django-admin.py startproject --template=https://gitlab.com/esg-dev/esg-django-app-template/repository/archive.zip --extension=py,rst,yml,conf <project_name>

{{ project_name|title }}
========================

Below you will find basic setup and deployment instructions for the {{ project_name }}
project. To begin you should have the following applications installed on your
local development system::
