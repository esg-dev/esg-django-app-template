#!/bin/bash

NAME="{{project_name}}"
DJANGODIR=/home/tal/apis/{{project_name}}
SOCKFILE=/home/tal/apis/{{project_name}}/run/gunicorn.sock
NUM_WORKERS=3
USER=tal
GROUP=www-data
DJANGO_SETTINGS_MODULE={{project_name}}.settings
DJANGO_WSGI_MODULE={{project_name}}.wsgi

echo "Starting $NAME as `whoami`"
cd $DJANGODIR
echo "`pwd`"
source ../.././.virtualenvs/{{project_name}}/bin/activate
echo "$PYTHONPATH"
export DJANGO_SETTINGS_MODULE={{project_name}}.settings
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
export IS_SERVER=True

exec ../.././.virtualenvs/{{project_name}}/bin/gunicorn {{project_name}}.wsgi:application --name $NAME --workers $NUM_WORKERS --user=$USER -b 0.0.0.0:8003 --log-level=debug --log-file=-
